<%@page language="java" contentType="text/html; charset=UTF-8"
          pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@ page import="ito_yuki.beans.Post"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
    	<div class="main-contents">
        	<div class="header">
            	<c:if test="${empty loginUser}">
                	<a href="login">ログイン</a>
                </c:if>
                <c:if test="${not empty errorMessages}">
                	<div class="errorMessages">
                    	<ul>
                        	<c:forEach items="${errorMessages}" var="message">
                            	<li><c:out value="${message}" />
                            </c:forEach>
                        </ul>
                    </div>
                    <c:remove var="errorMessages" scope="session" />
                 </c:if>
			</div>
            <c:if test="${not empty loginUser}">
            	<h1 align="center">-シンプルな掲示板-</h1>
            	★<a href="newpost">新規投稿</a><br>
                ★<a href="management">ユーザー管理</a><br>
                ★<a href="logout">ログアウト</a><br>
                <h4>カテゴリ検索</h4>
				<form method="get">
					<input type="text" name="keyword">
					<input type="submit" value="検索">
				</form>
				<h4>日付検索</h4>
				<form method="get">
					<input type="date" name="createDate" min="2018-01-01" max="2020-12-31">
					<input type="submit" value="検索">
				</form>
            	<div class="profile">
                	<div class="name">
                    	<h3>
                        	<c:out value="${loginUser.name}" /> でログイン中<br>
                        	<c:if test="${loginUser.loginId == 'tonkatu'}"><img src="http://cookingforest.net/images/4tonnkatu.jpg"></c:if>
                        	<c:if test="${loginUser.loginId == 'karaage'}"><img src="https://imgfp.hotp.jp/IMGH/95/50/P026809550/P026809550_368.jpg"></c:if>
                        	<c:if test="${loginUser.loginId == 'nikuman'}"><img src="https://img1.esimg.jp/image/food/02/39/01/1319526_relation_1_20170821112540.jpg?ts=1503282337000"></c:if>
                        </h3>
                    </div>
                </div>
            	<c:forEach var="userPost" items="${userPosts}">
             		<tr>
                		<td>******************************<br></td>
                    	<td><label for="title">●件名<br></label>${userPost.title}<br></td>
                    	<td><label for="text">●本文<br></label>${userPost.text}<br></td>
                    	<td><label for="category">●カテゴリー<br></label>${userPost.category}<br></td>
                    	<td><label for="createDate">●投稿日時<br></label>${userPost.createDate}<br></td>
                  		<td><label for="name">●投稿者<br></label>${userPost.name}<br></td>
                  		<td>
                  			<c:if test="${loginUser.loginId == userPost.postPerson}">
                        		<form action="postDelete" method="post">
                        			<input type="hidden" name="postId" value="${userPost.id}">
                        			<input type="submit" value="削除"/>
                        		</form>
                        	</c:if>
                        </td>
                    	<td><h5>みんなのコメント</h5></td>
                    	<td>
                    		<c:forEach var="coment" items="${coments}">
                        		<c:if test="${coment.postId == userPost.id}">
                        			<h4>${coment.name}: ${coment.coment}
                        			<c:if test="${loginUser.loginId == coment.postPerson}">
                        				<form action="coment" method="get">
                        					<input type="hidden" name="id" value="${coment.id}">
                        					<input type="submit" value="削除"/>
                        				</form>
                            		</c:if></h4>
                            	</c:if>
                        	</c:forEach>
                    	</td>
                  		<td>******************************<br></td>
                 	</tr>
                 <form action="coment" method="post">
                 	<label for="coment">コメント</label><br>
                    <textarea name="coment" cols="100" rows="5" class="post-box"></textarea><br>
                    <input type="hidden" name="postId" value="${userPost.id}">
                    <input type="submit" value="送信"/><br>
                 </form>
              </c:forEach>
           </c:if>
          </div>
          <div class="copylight">Copyright(c) ito_yuki</div>
	</body>
</html>