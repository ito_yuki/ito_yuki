<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${empty loginUser}">
				<h3>ログインしてください</h3>
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${not empty loginUser}">
				<c:if test="${not empty errorMessages}">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<form action="newpost" method="post">
					<h2>新規投稿</h2><br>
					<label for="title">件名(30文字以下)</label><br>
					<input name="title" id="title" /><br> <label for="text">本文(1000文字以下)</label><br>
					<textarea name="text" cols="100" rows="5" class="post-box"></textarea><br>
					<label for="category">カテゴリー(10文字以下)</label><br>
					<input name="category" id="category" /><br>
					<input name="title" value="${post.title}" type="hidden"/>
                	<input name="postPerson" value="${loginUser.loginId}" type="hidden"/>
					<input type="submit" value="投稿" /><br>
					★<a href="./">戻る</a>
				</form>
			</c:if>
			<div class="copyright">Copyright(c) ito_yuki</div>
		</div>
	</body>
</html>