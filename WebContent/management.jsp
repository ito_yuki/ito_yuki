<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${empty loginUser}">
				<h3>ログインしてください</h3>
				★<a href="login">ログイン</a>
			</c:if>
			<c:if test="${not empty loginUser}">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<c:if test="${loginUser.departmentId != 0}">
					<h3>権限がありません</h3>
				</c:if>
				<c:if test="${loginUser.departmentId == 0}">
					<h2>ユーザー管理</h2>
					★<a href="signup">新規登録</a><br>
					<c:forEach var="allUser" items="${allUser}">
						<h4>--------------------------------------</h4>
							●名前: ${allUser.name}<br>
							●ログインID: ${allUser.loginId}<br>
							●支店名: ${allUser.branchName}<br>
							●部署・役職: ${allUser.departmentName}<br>
							<a href="setting?allUserId=${allUser.id}">編集</a><br>
						<h4>--------------------------------------</h4>
              		</c:forEach>
					★<a href="./">戻る</a>
					<div class="copylight">Copyright(c) ito_yuki</div>
				</c:if>
			</c:if>
		</div>
	</body>
</html>