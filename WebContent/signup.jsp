<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー登録</title>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${empty loginUser}">
				<h3>ログインしてください</h3>
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${not empty loginUser}">
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>
				<c:if test="${loginUser.departmentId != 0}">
					<h3>権限がありません</h3>
				</c:if>
				<c:if test="${loginUser.departmentId == 0}">
					<form action="signup" method="post">
						<label for="loginId">ログインID</label>
						<input name="loginId" id="loginId" /><br>
						<label for="password">パスワード</label>
						<input name="password" type="password" id="password" /><br>
						<label for="password">パスワード(確認用)</label>
						<input name="passwordCheck" type="password" id="passwordCheck" /><br>
						<label for="name">名称</label>
						<input name="name" id="name" /><br>
						<label for="branchId">支店名</label>
						<select name="branchId">
							<option value="0">本社</option>
							<option value="1">支店A</option>
							<option value="2">支店B</option>
						</select><br>
						<label for="departmentId">部署・役職</label>
						<select name="departmentId">
							<option value="0">総務人事部</option>
							<option value="1">支店長</option>
							<option value="2">社員</option>
						</select><br>
						<input type="submit" value="登録" /><br>
						★<a href="management">戻る</a>
					</form>
				</c:if>
			</c:if>
			<div class="copyright">Copyright(c) ito_yuki</div>
		</div>
	</body>
</html>