package ito_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ito_yuki.beans.Post;
import ito_yuki.service.PostService;

@WebServlet({"/newpost"})
public class PostServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		request.getRequestDispatcher("/newpost.jsp").forward(request,response);
	}

	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<>();
		
//		if(request.getParameter("postId") != null) {
//			new PostService().delete(Integer.parseInt(request.getParameter("postId")));
//			response.sendRedirect("./");
//		}else{
			if(isValid(request,messages) == true){
				Post post = new Post();
				post.setTitle(request.getParameter("title"));
				post.setText(request.getParameter("text"));
				post.setCategory(request.getParameter("category"));
				post.setPostPerson(request.getParameter("postPerson"));

				new PostService().register(post);

				response.sendRedirect("./");
			}else{
				session.setAttribute("errorMessages",messages);
				response.sendRedirect("newpost");
			}
//		}
	}

	private boolean isValid(HttpServletRequest request,List<String> messages){
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isEmpty(title) == true){
			messages.add("件名を入力してください");
		}
		if(StringUtils.isEmpty(text) == true){
			messages.add("本文を入力してください");
		}
		if(StringUtils.isEmpty(category) == true){
			messages.add("カテゴリーを入力してください");
		}
		if(title.length() > 30 == true){
			messages.add("件名は30文字以内で入力してください");
		}
		if(text.length() > 1000 == true){
			messages.add("本文は1000文字以内で入力してください");
		}
		if(category.length() > 10 == true){
			messages.add("カテゴリーは10文字以下です");
		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}