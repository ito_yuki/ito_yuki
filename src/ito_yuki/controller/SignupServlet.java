package ito_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ito_yuki.beans.User;
import ito_yuki.service.UserService;

@WebServlet(urlPatterns={"/signup"})
public class SignupServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		List<String> newUsers = new ArrayList<>();
		
		HttpSession session = request.getSession();
		if(isValid(request,newUsers) == true){
			
			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
			
			new UserService().register(user);
			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessages",newUsers);
			response.sendRedirect("signup");
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> newUsers){
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String departmentId = request.getParameter("departmentId");
		
		if(StringUtils.isEmpty(loginId) == true){
			newUsers.add("ログインIDを入力してください");
		}
		if(!Pattern.matches("^[0-9a-zA-Z]+$", loginId)){
			newUsers.add("ログインIDは半角英数字で入力してください");
		}
		if(StringUtils.isEmpty(password) == true){
			newUsers.add("パスワードを入力してください");
		}
		if(request.getParameter("password").length() < 6 || request.getParameter("password").length() > 20){
			newUsers.add("パスワードは6文字以上20文字以下で入力してください");
		}
		if(!request.getParameter("password").equals(request.getParameter("passwordCheck"))){
			newUsers.add("パスワードが一致しません");
		}
		if(StringUtils.isEmpty(name) == true){
			newUsers.add("名称を入力してください");
		}
		if(request.getParameter("name").length() > 10){
			newUsers.add("名称は10文字以下で入力してください");
		}
		if(StringUtils.isEmpty(branchId) == true){
			newUsers.add("支店を入力してください");
		}
		if(StringUtils.isEmpty(departmentId) == true){
			newUsers.add("部署・役職を入力してください");
		}
		if(Integer.parseInt(request.getParameter("branchId")) != 0 && Integer.parseInt(request.getParameter("departmentId")) == 0){
			newUsers.add("人事総務部は本社のみです");
		}
		if(newUsers.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}