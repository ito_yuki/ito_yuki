package ito_yuki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ito_yuki.service.PostService;

@WebServlet({"/postDelete"})
public class PostDeleteServlet extends HttpServlet{

	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{

		new PostService().delete(Integer.parseInt(request.getParameter("postId")));
		response.sendRedirect("./");
	}

}

