package ito_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ito_yuki.beans.User;
import ito_yuki.service.LoginService;

@WebServlet(urlPatterns={"/login"})
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		//リクエストパラメータの取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		
		//ログイン処理
		LoginService loginService = new LoginService();
        User user = loginService.login(loginId,password);

        HttpSession session = request.getSession();
        if(user != null){
        	session.setAttribute("beforeEncryptionPassword",password);
        	session.setAttribute("loginUser",user);
        	response.sendRedirect("./");
        }else{
        	List<String> messages = new ArrayList<>();
            messages.add("ログインに失敗しました。");
            session.setAttribute("errorMessages",messages);
            response.sendRedirect("login");
        }
	}
}