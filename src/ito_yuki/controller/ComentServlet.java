package ito_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ito_yuki.beans.Coment;
import ito_yuki.beans.User;
import ito_yuki.service.ComentService;

@WebServlet(urlPatterns={"/coment"})
public class ComentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		new ComentService().delete(Integer.parseInt(request.getParameter("id")));
		response.sendRedirect("./");
	}
	
	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<>();

        if(isValid(request,messages) == true){
        	User user = (User) session.getAttribute("loginUser");

        	Coment coment = new Coment();
        	coment.setName(user.getName());
        	coment.setPostId(Integer.parseInt(request.getParameter("postId")));
        	coment.setComent(request.getParameter("coment"));
        	coment.setPostPerson(user.getLoginId());

        	new ComentService().register(coment);
        	
        	response.sendRedirect("./");
        }else{
            session.setAttribute("errorMessages",messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages){

        String coment = request.getParameter("coment");

        if(StringUtils.isEmpty(coment) == true){
            messages.add("コメントは空のまま登録できません");
        }
        if(messages.size() == 0){
            return true;
        }else{
            return false;
        }
    }
}