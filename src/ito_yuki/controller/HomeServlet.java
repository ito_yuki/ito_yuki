package ito_yuki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import ito_yuki.beans.UserComent;
import ito_yuki.beans.UserPost;
import ito_yuki.service.ComentService;
import ito_yuki.service.PostService;

@WebServlet(urlPatterns={"/index.jsp"})
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
									throws IOException,ServletException{
		
		List<UserPost> userPosts = null;
		if(StringUtils.isNotEmpty(request.getParameter("keyword"))){
			userPosts = new PostService().search(request.getParameter("keyword"));
		}
		else if(StringUtils.isNotEmpty(request.getParameter("createDate"))){
			userPosts = new PostService().searchDate(request.getParameter("createDate"));
		}
		else{
			userPosts = new PostService().getUserPosts();
		}
		
		List<UserComent> userComents = new ComentService().getUserComents();
        
		request.setAttribute("userPosts",userPosts);
		request.setAttribute("coments",userComents);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}