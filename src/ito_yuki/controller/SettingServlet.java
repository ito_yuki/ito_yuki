package ito_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ito_yuki.beans.User;
import ito_yuki.service.UserService;

@WebServlet(urlPatterns={"/setting"})
public class SettingServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		
		request.setAttribute("userProperty",new UserService().getUser(Integer.parseInt(request.getParameter("allUserId"))));
		request.getRequestDispatcher("/settings.jsp").forward(request,response);
	}
	
	protected void doPost(HttpServletRequest request,
							HttpServletResponse response)
								throws IOException,ServletException{
		List<String> messages = new ArrayList<>();
		HttpSession session = request.getSession();
		
		if(isValid(request,messages) == true){
			
			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
			if(request.getParameter("password").isEmpty()){
				new UserService().updateNoPassword(user,Integer.parseInt(request.getParameter("id")));
			}else{
				user.setPassword(request.getParameter("password"));
				new UserService().update(user,Integer.parseInt(request.getParameter("id")));
			}
			
			request.getRequestDispatcher("/management").forward(request, response);
		}else{
			session.setAttribute("errorMessages",messages);
			response.sendRedirect("setting");
		}
	}
	
	private boolean isValid(HttpServletRequest request, List<String> messages){
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branchId");
		String departmentId = request.getParameter("departmentId");
		
		if(StringUtils.isEmpty(loginId) == true){
			messages.add("ログインIDを入力してください");
		}
		if(StringUtils.isEmpty(name) == true){
			messages.add("名称を入力してください");
		}
		if(StringUtils.isEmpty(branchId) == true){
			messages.add("支店を入力してください");
		}
		if(StringUtils.isEmpty(departmentId) == true){
			messages.add("部署・役職を入力してください");
		}
		if(!request.getParameter("password").equals(request.getParameter("passwordCheck"))){
			messages.add("パスワードが一致しません");
		}
		if(Integer.parseInt(request.getParameter("branchId")) != 0 && Integer.parseInt(request.getParameter("departmentId")) == 0){
			messages.add("人事総務部は本社のみです");
		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}