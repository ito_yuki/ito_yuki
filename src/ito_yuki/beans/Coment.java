package ito_yuki.beans;

import java.io.Serializable;
import java.util.Date;

public class Coment implements Serializable{
	private int id;
	private String name;
	private int postId;
	private String coment;
	private Date createDate;
	private String postPerson;
	
	public Coment(){
		
	}

	public String getComent() {
		return coment;
	}
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPostId() {
		return postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createdDate) {
		this.createDate = createdDate;
	}

	public String getPostPerson() {
		return postPerson;
	}

	public void setPostPerson(String postPerson) {
		this.postPerson = postPerson;
	}
	
	
}
