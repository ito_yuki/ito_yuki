package ito_yuki.beans;

import java.io.Serializable;
import java.util.Date;

public class UserComent implements Serializable{
	private int id;
	private int postId;
	private String coment;
	private Date createDate;
	private String postPerson;
	private String name;
	private String loginId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getComent() {
		return coment;
	}
	public void setComent(String coment) {
		this.coment = coment;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getPostPerson() {
		return postPerson;
	}
	public void setPostPerson(String postPerson) {
		this.postPerson = postPerson;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
}
