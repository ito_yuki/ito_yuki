package ito_yuki.beans;

import java.io.Serializable;
import java.util.Date;

public class Post implements Serializable{
	private int id;
	private String title;
	private String text;
	private String category;
	private Date createDate;
	private String postPerson;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getPostPerson() {
		return postPerson;
	}
	public void setPostPerson(String postPerson) {
		this.postPerson = postPerson;
	}
	
}
