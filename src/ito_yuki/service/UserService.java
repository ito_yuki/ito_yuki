package ito_yuki.service;

import static ito_yuki.utils.CloseableUtil.*;
import static ito_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ito_yuki.beans.User;
import ito_yuki.beans.UserBranchDepartment;
import ito_yuki.dao.UserBranchDepartmentDao;
import ito_yuki.dao.UserDao;
import ito_yuki.utils.CipherUtil;

public class UserService{

    public void register(User user){

        Connection connection = null;
        try{
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        }catch(RuntimeException e){
            rollback(connection);
            throw e;
        }catch(Error e){
            rollback(connection);
            throw e;
        }finally{
            close(connection);
        }
    }

    public List<UserBranchDepartment> getUser(){
    	Connection connection = null;
    	try{
    		connection = getConnection();
    		UserBranchDepartmentDao userBranchDepartmentDao = 
    									new UserBranchDepartmentDao();
    		List<UserBranchDepartment> ret = userBranchDepartmentDao.
    							getUserBranchDepartment(connection);
    		
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
    }
    
    public UserBranchDepartment getUser(int id){
    	Connection connection = null;
    	try{
    		connection = getConnection();
    		UserDao userDao = new UserDao();
    		UserBranchDepartment ret = userDao.searchUser(connection,id);
    		
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
    }


    public void update(User user,int id){
		Connection connection = null;
		try{
			connection = getConnection();
			user = new User();
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection,id);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
    
    public void updateNoPassword(User user,int id){
		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.updateNoPassword(connection,user,id);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}


//	public void update(User user){
//		Connection connection = null;
//		try{
//			connection = getConnection();
//			String encPassword = CipherUtil.encrypt(user.getPassword());
//			user.setPassword(encPassword);
//
//			UserDao userDao = new UserDao();
//			userDao.update(connection);
//
//			commit(connection);
//		}catch(RuntimeException e){
//			rollback(connection);
//			throw e;
//		}catch(Error e){
//			rollback(connection);
//			throw e;
//		}finally{
//			close(connection);
//		}
//	}
}