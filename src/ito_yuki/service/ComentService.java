package ito_yuki.service;

import static ito_yuki.utils.CloseableUtil.*;
import static ito_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ito_yuki.beans.Coment;
import ito_yuki.beans.UserComent;
import ito_yuki.dao.ComentDao;
import ito_yuki.dao.UserComentDao;

public class ComentService {
	public void register(Coment coment){
		Connection connection = null;
		try{
			connection = getConnection();
			ComentDao comentDao = new ComentDao();
			comentDao.insert(connection,coment);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<Coment> getComent(){
		Connection connection = null;
		try{
			connection = getConnection();
			ComentDao comentDao = new ComentDao();
			List<Coment> coment = comentDao.getComent(connection);
			commit(connection);
			return coment;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void delete(int id){
		Connection connection = null;
		try{
			connection = getConnection();
			ComentDao comentDao = new ComentDao();
			comentDao.delete(connection,id);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
	
    public List<UserComent> getUserComents(){
    	Connection connection = null;
    	try{
    		connection = getConnection();
    		UserComentDao userComentDao = new UserComentDao();
    		List<UserComent> ret = userComentDao.getUserComents(connection);
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
    }

}