package ito_yuki.service;

import static ito_yuki.utils.CloseableUtil.*;
import static ito_yuki.utils.DBUtil.*;

import java.sql.Connection;

import ito_yuki.beans.User;
import ito_yuki.dao.UserDao;
import ito_yuki.utils.CipherUtil;

public class LoginService {
	public User login(String loginId, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection,loginId,encPassword);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}