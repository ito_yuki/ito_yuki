package ito_yuki.service;

import static ito_yuki.utils.CloseableUtil.*;
import static ito_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ito_yuki.beans.Post;
import ito_yuki.beans.UserPost;
import ito_yuki.dao.PostDao;
import ito_yuki.dao.UserPostDao;


public class PostService{
	public void register(Post post){
        Connection connection = null;
        try{
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection,post);
            commit(connection);
        }catch(RuntimeException e){
            rollback(connection);
            throw e;
        }catch(Error e){
            rollback(connection);
            throw e;
        }finally{
            close(connection);
        }
    }
	
	public List<Post> getPost(){
		Connection connection = null;
		try{
			connection = getConnection();
			PostDao postDao = new PostDao();
			List<Post> post = postDao.getPost(connection);
			commit(connection);
			return post;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
	
	public void delete(int id){
		Connection connection = null;
		try{
			connection = getConnection();
			PostDao postDao = new PostDao();
			postDao.delete(connection,id);
			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}
	
    public List<UserPost> getUserPosts(){
    	Connection connection = null;
    	try{
    		connection = getConnection();
    		UserPostDao userPostDao = new UserPostDao();
    		List<UserPost> ret = userPostDao.getUserPosts(connection);
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
    }

	public List<UserPost> search(String keyword){
		Connection connection = null;
    	try{
    		connection = getConnection();
    		UserPostDao UserPostDao = new UserPostDao();
    		List<UserPost> ret = UserPostDao.getSearchUserPosts(connection,keyword);
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
	}
	
	public List<UserPost> searchDate(String createDate){
		Connection connection = null;
    	try{
    		connection = getConnection();
    		UserPostDao UserPostDao = new UserPostDao();
    		List<UserPost> ret = UserPostDao.getSearchUserPostsDate(connection,createDate);
    		commit(connection);
    		return ret;
    	}catch(RuntimeException e){
    		rollback(connection);
    		throw e;
    	}catch(Error e){
    		rollback(connection);
    		throw e;
    	}finally{
    		close(connection);
    	}
	}

}