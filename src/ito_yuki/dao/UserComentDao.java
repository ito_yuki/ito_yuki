package ito_yuki.dao;

import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.UserComent;
import ito_yuki.exception.SQLRuntimeException;

public class UserComentDao{

    public List<UserComent> getUserComents(Connection connection){

        PreparedStatement ps = null;
        try{
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("coments.id, ");
            sql.append("coments.coment, ");
            sql.append("coments.create_date, ");
            sql.append("coments.post_id, ");
            sql.append("coments.post_person, ");
            sql.append("users.name ");
            sql.append("FROM coments ");
            sql.append("INNER JOIN users ");
            sql.append("ON coments.post_person = users.login_id ");
            sql.append("ORDER BY coments.create_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComent> ret = toUserComentList(rs);
            return ret;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
            close(ps);
        }
    }
    
    private List<UserComent> toUserComentList(ResultSet rs) throws SQLException{

        List<UserComent> ret = new ArrayList<>();
        try{
            while(rs.next()){
            	int id = rs.getInt("id");
            	String postPerson = rs.getString("post_person");
                String coment = rs.getString("coment");
                String name = rs.getString("name");
                int postId = rs.getInt("post_id");
                Timestamp createDate = rs.getTimestamp("create_date");

                UserComent userComent = new UserComent();
                userComent.setId(id);
                userComent.setPostPerson(postPerson);
                userComent.setPostId(postId);
                userComent.setComent(coment);
                userComent.setName(name);
                userComent.setCreateDate(createDate);

                ret.add(userComent);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
}
