package ito_yuki.dao;


import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.UserBranchDepartment;
import ito_yuki.exception.SQLRuntimeException;

public class UserBranchDepartmentDao{

    public List<UserBranchDepartment> getUserBranchDepartment(Connection connection){

        PreparedStatement ps = null;
        try{
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.login_id, ");
            sql.append("users.password, ");
            sql.append("users.name, ");
            sql.append("users.branch_id, ");
            sql.append("users.department_id, ");
            sql.append("branchs.branch_name, ");
            sql.append("departments.department_name ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branchs ");
            sql.append("ON users.branch_id = branchs.branch_id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.department_id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserBranchDepartment> ret = toUserBranchDepartmentList(rs);
            return ret;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
            close(ps);
        }
    }
    
    private List<UserBranchDepartment> toUserBranchDepartmentList(ResultSet rs) throws SQLException{

        List<UserBranchDepartment> ret = new ArrayList<>();
        try{
            while(rs.next()){
            	int id = rs.getInt("id");
            	String loginId = rs.getString("login_id");
            	String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");
                String branchName = rs.getString("branch_name");
                String departmentName = rs.getString("department_name");

                UserBranchDepartment user = new UserBranchDepartment();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setBranchName(branchName);
                user.setDepartmentName(departmentName);

                ret.add(user);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
}
