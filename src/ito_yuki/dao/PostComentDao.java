//package ito_yuki.dao;
//
//
//import static ito_yuki.utils.CloseableUtil.*;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.List;
//
//import ito_yuki.beans.PostComent;
//import ito_yuki.exception.SQLRuntimeException;
//
//public class PostComentDao{
//
//    public List<PostComent> getPostComents(Connection connection){
//
//        PreparedStatement ps = null;
//        try{
//            StringBuilder sql = new StringBuilder();
//            sql.append("SELECT ");
//            sql.append("coments.coment, ");
//            sql.append("coments.create_date, ");
//            sql.append("FROM posts ");
//            sql.append("INNER JOIN coments ");
//            sql.append("ON posts.id = coments.post_id ");
//            sql.append("ORDER BY posts.create_date DESC ");
//
//            ps = connection.prepareStatement(sql.toString());
//
//            ResultSet rs = ps.executeQuery();
//            List<PostComent> ret = toPostComentList(rs);
//            return ret;
//        }catch(SQLException e){
//            throw new SQLRuntimeException(e);
//        }finally{
//            close(ps);
//        }
//    }
//    
//    private List<PostComent> toPostComentList(ResultSet rs) throws SQLException{
//
//        List<PostComent> ret = new ArrayList<>();
//        try{
//            while(rs.next()){
//                String coment = rs.getString("coment");
//                Timestamp createDate = rs.getTimestamp("create_date");
//
//                PostComent postComent = new PostComent();
//                postComent.setComent(coment);
//                postComent.setCreateDate(createDate);
//                
//                ret.add(postComent);
//            }
//            return ret;
//        }finally{
//            close(rs);
//        }
//    }
//
//}
