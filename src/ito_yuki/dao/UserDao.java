package ito_yuki.dao;


import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.User;
import ito_yuki.beans.UserBranchDepartment;
import ito_yuki.exception.NoRowsUpdatedRuntimeException;
import ito_yuki.exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // department_id
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			
			ps.executeUpdate();
		}catch(SQLException e){
			e.getMessage();
		}finally{
			close(ps);
		}
	}

	public User getUser(Connection connection,
							String loginId,String password){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";
			
			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
            ps.setString(2, password);
            
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if(userList.isEmpty() == true){
                return null;
            }else if(2 <= userList.size()){
                throw new IllegalStateException("2 <= userList.size()");
            }else{
                return userList.get(0);
            }
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
            close(ps);
        }
	}
	
	private List<User> toUserList(ResultSet rs) throws SQLException{
		List<User> userProperties = new ArrayList<>();
		try{
			while(rs.next()){
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");

                User user = new User();
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                
                userProperties.add(user);
			}
			return userProperties;
		}finally{
			close(rs);
		}
	}
	
	public List<User> getAllUser(Connection connection){
    	PreparedStatement ps = null;
    	try{
    		String sql = "SELECT * FROM users";

    		ps = connection.prepareStatement(sql);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toAllUserList(rs);
			return userList;
    	}catch(SQLException e){
    		throw new SQLRuntimeException(e);
    	}finally{
    		close(ps);
    	}
    }
	
	private List<User> toAllUserList(ResultSet rs) throws SQLException{
		
		List<User> userProperties = new ArrayList<>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                
                userProperties.add(user);
			}
			return userProperties;
		}finally{
			close(rs);
		}
	}

	
	public void update(Connection connection,int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			User user = new User();
			ps.setString(1,user.getLoginId());
			ps.setString(2,user.getPassword());
			ps.setString(3,user.getName());
			ps.setInt(4,user.getBranchId());
			ps.setInt(5,user.getDepartmentId());
			ps.setInt(6,id);
			
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	
	public void updateNoPassword(Connection connection,User user,int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,user.getLoginId());
			ps.setString(2,user.getName());
			ps.setInt(3,user.getBranchId());
			ps.setInt(4,user.getDepartmentId());
			ps.setInt(5,id);
			
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	
	public UserBranchDepartment searchUser(Connection connection,int id){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1,id);
			ResultSet rs = ps.executeQuery();
			List<UserBranchDepartment> allUsers = toAllUsers(rs);
			return allUsers.get(0);
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	
	private List<UserBranchDepartment> toAllUsers(ResultSet rs) throws SQLException{
		
		List<UserBranchDepartment> userProperties = new ArrayList<>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");

                UserBranchDepartment user = new UserBranchDepartment();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                
                userProperties.add(user);
			}
			return userProperties;
		}finally{
			close(rs);
		}
	}


}