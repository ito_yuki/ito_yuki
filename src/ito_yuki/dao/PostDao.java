package ito_yuki.dao;

import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.Post;
import ito_yuki.exception.SQLRuntimeException;

public class PostDao {
	public void insert(Connection connection,Post post){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("title");
			sql.append(", text");
			sql.append(", category");
			sql.append(", create_date");
			sql.append(", post_person");
			sql.append(") VALUES (");
			sql.append("?"); // title
			sql.append(", ?"); // text
			sql.append(", ?"); // category
			sql.append(", CURRENT_TIMESTAMP"); // create_date
			sql.append(", ?"); // post_person
			sql.append(")");
			
			ps = connection.prepareStatement(sql.toString());
			
			ps.setString(1,post.getTitle());
			ps.setString(2,post.getText());
			ps.setString(3,post.getCategory());
			ps.setString(4,post.getPostPerson());
			ps.executeUpdate();
		}catch(SQLException e){
			e.getMessage();
		}finally{
			close(ps);
		}
	}
	
	public List<Post> getPost(Connection connection){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM posts";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Post> posts = toPostList(rs);
			return posts;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
    
	private List<Post> toPostList(ResultSet rs) throws SQLException{
        List<Post> ret = new ArrayList<>();
        try{
            while(rs.next()){
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createDate = rs.getTimestamp("create_date");
                String postPerson = rs.getString("post_person");

                Post post = new Post();
                post.setId(id);
                post.setTitle(title);
                post.setText(text);
                post.setCategory(category);
                post.setCreateDate(createDate);
                post.setPostPerson(postPerson);

                ret.add(post);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
	
	public boolean delete(Connection connection,int id){
		PreparedStatement ps = null;
		boolean judge = true;
		try{
			String sql = "DELETE FROM posts WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1,id);
			ps.executeUpdate();
		}catch(SQLException e){
			e.getMessage();
		}finally{
			close(ps);
		}
		return judge;
	}

	public List<Post> searchPost(Connection connection,String keyword){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM posts WHERE category like ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1,"%" + keyword + "%");
			ResultSet rs = ps.executeQuery();
			List<Post> posts = toPostList(rs);
			return posts;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	
	public List<Post> getSearchUserPosts(Connection connection,String keyword){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM posts WHERE category like ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1,"%" + keyword + "%");
			ResultSet rs = ps.executeQuery();
			List<Post> posts = toPostList(rs);
			return posts;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
		
	}

}