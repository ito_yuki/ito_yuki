package ito_yuki.dao;


import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.UserPost;
import ito_yuki.exception.SQLRuntimeException;

public class UserPostDao{

    public List<UserPost> getUserPosts(Connection connection){

        PreparedStatement ps = null;
        try{
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id, ");
            sql.append("posts.title, ");
            sql.append("posts.text, ");
            sql.append("posts.category, ");
            sql.append("posts.create_date, ");
            sql.append("posts.post_person, ");
            sql.append("users.name, ");
            sql.append("users.login_id ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.post_person = users.login_id ");
            sql.append("ORDER BY posts.create_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        }catch(SQLException e){
            throw new SQLRuntimeException(e);
        }finally{
            close(ps);
        }
    }
    
    private List<UserPost> toUserPostList(ResultSet rs) throws SQLException{

        List<UserPost> ret = new ArrayList<>();
        try{
            while(rs.next()){
            	String loginId = rs.getString("login_id");
            	int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createDate = rs.getTimestamp("create_date");
                String postPerson = rs.getString("post_person");
                String name = rs.getString("name");

                UserPost userPost = new UserPost();
                userPost.setLoginId(loginId);
                userPost.setId(id);
                userPost.setTitle(title);
                userPost.setText(text);
                userPost.setCategory(category);
                userPost.setCreateDate(createDate);
                userPost.setName(name);
                userPost.setPostPerson(postPerson);

                ret.add(userPost);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
    
    public List<UserPost> getSearchUserPosts(Connection connection,String keyword){
    	
    	PreparedStatement ps = null;
		try{
			String sql = "SELECT posts.id,posts.title,posts.text,posts.category,posts.create_date,posts.post_person,"
							+ "users.name,users.login_id FROM posts INNER JOIN users ON posts.post_person = users.login_id "
								+ "WHERE category like ? ORDER BY posts.create_date DESC";
			ps = connection.prepareStatement(sql);
			ps.setString(1,"%" + keyword + "%");
			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toSearchUserPostList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
    }
    
    private List<UserPost> toSearchUserPostList(ResultSet rs)throws SQLException{

        List<UserPost> ret = new ArrayList<>();
        try{
            while(rs.next()){
            	String loginId = rs.getString("login_id");
            	int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createDate = rs.getTimestamp("create_date");
                String postPerson = rs.getString("post_person");
                String name = rs.getString("name");

                UserPost userPost = new UserPost();
                userPost.setLoginId(loginId);
                userPost.setId(id);
                userPost.setTitle(title);
                userPost.setText(text);
                userPost.setCategory(category);
                userPost.setCreateDate(createDate);
                userPost.setName(name);
                userPost.setPostPerson(postPerson);

                ret.add(userPost);
            }
            return ret;
        }finally{
            close(rs);
        }
    }
    
    public List<UserPost> getSearchUserPostsDate(Connection connection,String createDate){
    	
    	PreparedStatement ps = null;
		try{
			String sql = "SELECT posts.id,posts.title,posts.text,posts.category,posts.create_date,posts.post_person,"
							+ "users.name,users.login_id FROM posts INNER JOIN users ON posts.post_person = users.login_id "
								+ "WHERE posts.create_date BETWEEN ? AND ? ORDER BY posts.create_date DESC";
			ps = connection.prepareStatement(sql);
			ps.setString(1,createDate + " 00:00:00");
			ps.setString(2,createDate + " 23:59:59");
			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toSearchDateList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
    }
    
    private List<UserPost> toSearchDateList(ResultSet rs)throws SQLException{

        List<UserPost> ret = new ArrayList<>();
        try{
            while(rs.next()){
            	String loginId = rs.getString("login_id");
            	int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createDate = rs.getTimestamp("create_date");
                String postPerson = rs.getString("post_person");
                String name = rs.getString("name");

                UserPost userPost = new UserPost();
                userPost.setLoginId(loginId);
                userPost.setId(id);
                userPost.setTitle(title);
                userPost.setText(text);
                userPost.setCategory(category);
                userPost.setCreateDate(createDate);
                userPost.setName(name);
                userPost.setPostPerson(postPerson);

                ret.add(userPost);
            }
            return ret;
        }finally{
            close(rs);
        }
    }

}
