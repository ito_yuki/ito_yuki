package ito_yuki.dao;



import static ito_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ito_yuki.beans.Coment;
import ito_yuki.exception.SQLRuntimeException;



public class ComentDao{

	public void insert(Connection connection,Coment coment){
		PreparedStatement ps = null;

		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO coments ( ");
			sql.append("name");
			sql.append(", post_id");
			sql.append(", coment");
			sql.append(", create_date");
			sql.append(", post_person");
			sql.append(") VALUES (");
			sql.append("?"); // name
			sql.append(", ?"); // post_id
			sql.append(", ?"); // coment
			sql.append(", CURRENT_TIMESTAMP"); // create_date
			sql.append(", ?"); // post_person
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, coment.getName());
			ps.setInt(2,coment.getPostId());
			ps.setString(3,coment.getComent());
			ps.setString(4,coment.getPostPerson());

			ps.executeUpdate();
		}catch(SQLException e){
			e.getMessage();
		}finally{
			close(ps);
		}
	}

	public List<Coment> getComent(Connection connection){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM coments ";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			List<Coment> userComents = toUserComentList(rs);
			return userComents;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	private List<Coment> toUserComentList(ResultSet rs) throws SQLException{
		List<Coment> ret = new ArrayList<>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int postId = rs.getInt("post_id");
				String coment = rs.getString("coment");
				Timestamp createDate = rs.getTimestamp("create_date");
				String postPerson = rs.getString("post_person");

				Coment comentList = new Coment();
				comentList.setId(id);
				comentList.setName(name);
				comentList.setPostId(postId);
				comentList.setComent(coment);
				comentList.setCreateDate(createDate);
				comentList.setPostPerson(postPerson);

				ret.add(comentList);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public boolean delete(Connection connection,int id){
		PreparedStatement ps = null;
		boolean judge = true;
		try{
			String sql = "DELETE FROM coments WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1,id);

			ps.executeUpdate();
		}catch(SQLException e){
			e.getMessage();
		}finally{
			close(ps);
		}
		return judge;
	}
}
